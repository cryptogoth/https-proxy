const fs = require('fs');
const https = require('https');
const WebSocket = require('ws');

require('https').globalAgent.options.ca = require('ssl-root-cas/latest').create();

const server = https.createServer({
    key  : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/privkey.pem', 'utf8'),
    cert : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/cert.pem', 'utf8'),
    ca : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/chain.pem', 'utf8'),
});

// Create a wss server to listen for public connections
const wss = new WebSocket.Server({ server });

// Repeat messages from public wss socket to local ws
wss.on('connection', function connection(ws) {

  // Create a loopback ws client to a local websocket server
  const wsl = new WebSocket('ws://localhost:8549', {
    origin: 'https://ganache.arcology.nyc:8549'
  })

  // Diagnostic console output on connection and disconnection
  wsl.on('open', function open() {
    console.log('connected to loopback ws server on 8549', JSON.stringify(ws))
    ws.on('message', function incoming(message) {
      console.log('wss received: %s', message);
      wsl.send(message);
    });

  })

  wsl.on('close', function close() {
    console.log('disconnected from loopback ws server on 8546')
  })

  // Repeat messages from local ws to public wss
  wsl.on('message', function incoming(data) {
    console.log('ws received: %s', data)
    wss.send(data)
  })

});

server.listen(8550);
