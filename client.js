const WebSocket = require('ws');

//const ws = new WebSocket('wss://ganache.arcology.nyc:8080', {
//	origin: 'https://ganache.arcology.nyc:8080'
//});
const ws = new WebSocket('ws://localhost:8549', {
	origin: 'http://localhost:8080'
});

ws.on('open', function open() {
  console.log('connected');
  //ws.send(Date.now());
});

ws.on('close', function close() {
  console.log('disconnected');
});

ws.on('message', function incoming(data) {
  console.log(`Data received ${data}`)
  console.log(`Roundtrip time: ${Date.now() - data} ms`);
/*
  setTimeout(function timeout() {
    ws.send(Date.now());
  }, 500);
*/
});
