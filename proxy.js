const httpProxy = require('http-proxy')
const fs = require('fs')

require('https').globalAgent.options.ca = require('ssl-root-cas/latest').create();

httpProxy.createServer({
  target: {
    host : 'localhost',
    port : 7000,
  },
  ssl: {
    key  : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/privkey.pem', 'utf8'),
    cert : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/cert.pem', 'utf8'),
    ca : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/chain.pem', 'utf8'),
  },
  secure: true,
}).listen(7001);

httpProxy.createServer({
  target: {
    host : 'localhost',
    port : 7000,
  },
  ssl: {
    key  : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/privkey.pem', 'utf8'),
    cert : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/cert.pem', 'utf8'),
    ca : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/chain.pem', 'utf8'),
  },
  secure: true,
}).listen(443);

httpProxy.createServer({
  target: {
    host : 'localhost',
    port : 8548 ,
  },
  ssl: {
    key  : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/privkey.pem', 'utf8'),
    cert : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/cert.pem', 'utf8'),
    ca : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/chain.pem', 'utf8'),
  },
  secure: true,
}).listen(8549);

httpProxy.createServer({
  target: {
    host : 'localhost',
    port : 8545 ,
  },
  ssl: {
    key  : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/privkey.pem', 'utf8'),
    cert : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/cert.pem', 'utf8'),
    ca : fs.readFileSync('/etc/letsencrypt/live/ganache.arcology.nyc/chain.pem', 'utf8'),
  },
  secure: true,
}).listen(8547);
